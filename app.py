from flask import Flask, render_template, jsonify
from random import sample
import json
import random
import requests # Import du module requests

app = Flask(__name__)

@app.route('/')
def accueil():
    response = requests.get('https://allosaurus.delahayeyourself.info/api/dinosaurs/')
    content= json.loads(response.content.decode('utf-8'))
    return render_template('index.html', content=content)

@app.route('/dinosaurs/<string:nom>')
def dinosaure(nom):
    response = requests.get('https://allosaurus.delahayeyourself.info/api/dinosaurs/' + nom)
    response2 = requests.get('https://allosaurus.delahayeyourself.info/api/dinosaurs/')
    content = json.loads(response.content.decode('utf-8'))
    content2 = json.loads(response2.content.decode('utf-8'))
    aleatoire = random.sample(content2, 3)
    return render_template('dinosaure.html', content=content, aleatoire=aleatoire)



