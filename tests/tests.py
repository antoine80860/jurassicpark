import os
import pytest
from app import app
from flask import url_for

@pytest.fixture
def client():
    app.config['TESTING'] = True
    app.config['SERVER_NAME'] = 'test'
    client = app.test_client()
    with app.app_context():
        pass
    app.app_context().push()
    yield client


def test_homepage(client):
    response = client.get('/')
    assert 200 == response.status_code



def test_dinosaure(client):
    response = client.get('/dinosaurs/dilophosaurus')
    assert 200 == response.status_code
    response = client.get('/dinosaurs/velociraptor')
    assert 200 == response.status_code
    response = client.get('/dinosaurs/brachiosaurus')
    assert 200 == response.status_code
    response = client.get('/dinosaurs/parasaurolophus')
    assert 200 == response.status_code
    response = client.get('/dinosaurs/triceratops')
    assert 200 == response.status_code
    response = client.get('/dinosaurs/tyrannosaurus-rex')
    assert 200 == response.status_code
    response = client.get('/dinosaurs/gallimimus')
    assert 200 == response.status_code
